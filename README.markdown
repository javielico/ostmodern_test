# [Ostmodern_Test](https://javi.pro)

[![Gem Version](https://img.shields.io/gem/v/jekyll.svg)][ruby-gems]
[![Linux Build Status](https://img.shields.io/travis/jekyll/jekyll/master.svg?label=Linux%20build)][travis]
[![Windows Build status](https://img.shields.io/appveyor/ci/jekyll/jekyll/master.svg?label=Windows%20build)][appveyor]
[![Test Coverage](https://img.shields.io/codeclimate/coverage/github/jekyll/jekyll.svg)][coverage]
[![Code Climate](https://img.shields.io/codeclimate/github/jekyll/jekyll.svg)][codeclimate]
[![Dependency Status](https://img.shields.io/gemnasium/jekyll/jekyll.svg)][gemnasium]
[![Security](https://hakiri.io/github/jekyll/jekyll/master.svg)][hakiri]

[ruby-gems]: https://rubygems.org/gems/jekyll
[gemnasium]: https://gemnasium.com/jekyll/jekyll
[codeclimate]: https://codeclimate.com/github/jekyll/jekyll
[coverage]: https://codeclimate.com/github/jekyll/jekyll/coverage
[hakiri]: https://hakiri.io/github/jekyll/jekyll/master
[travis]: https://travis-ci.org/jekyll/jekyll
[appveyor]: https://ci.appveyor.com/project/jekyll/jekyll/branch/master

This test has been made using Jekyll is a simple, static site generator perfect for personal, project, or organization sites. Think of it like a file-based CMS, without all the complexity. Jekyll takes your content, renders Markdown and Liquid templates, and spits out a complete, static website ready to be served by Apache, Nginx or another web server.

## How to run this test for Ostmodern?

* [Install](https://jekyllrb.com/docs/installation/) the gem
* Read up about its [Usage](https://jekyllrb.com/docs/usage/) and [Configuration](https://jekyllrb.com/docs/configuration/)
* Take a gander at some existing [Sites](https://wiki.github.com/jekyll/jekyll/sites)
* Have questions? Check out the official forum community [Jekyll Talk](https://talk.jekyllrb.com/) or [`#jekyll` on irc.freenode.net](https://botbot.me/freenode/jekyll/) or just simply drop me an email to hithere@javi.pro

## Having trouble with OS X El Capitan?

See: https://jekyllrb.com/docs/troubleshooting/#jekyll-amp-mac-os-x-1011

## License

See the [LICENSE](https://github.com/jekyll/jekyll/blob/master/LICENSE) file.
